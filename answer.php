<?php
include "db.php";
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Porao FAQ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <div class="page-header">
        <h1>Porao FAQ <small>Porao Frequently Asked Questions</small></h1>
        <h3>Hi, <?php echo $_SESSION["name"];?></h3>
    </div>

    <!-- Bootstrap FAQ - START -->
    <div class="container">
        <br />
        <br />
        <br />
        <?php
            //Update answer in FAQ Table
            if(isset($_POST["submit"])){
                $id = $_POST["faqid"];
                $answer = $_POST["answer"];

                $sql = "update faq_questions set answer = '$answer' where id='$id'";
                $run = $conn->prepare($sql);
                $run->execute();
            }
        ?>
            <!--Important question-->
            <div class="faqHeader">Important Question</div>

            <?php
            //Show FAQ which is not answered
            $sql = "select * from faq_questions where answer is NULL";
            $run = $conn->prepare($sql);
            $run->execute();

            if($run->rowCount() > 0){
                while ($row = $run->fetch(PDO::FETCH_ASSOC)){
                    $question = $row["questions"];
                    $answer = $row["answer"];
                    $name = $row["user_name"];
                    $email = $row["user_email"];
                    $faqid = $row["id"];
                    $vote = $row["vote_count"];

                    echo '
                          <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#">'.$question.' </a>
                                    <p><small><b>'.$vote.' people voted for this question.</b></small></p>
                                </h4>
                                <form action="answer.php" method="post">
                                    <input type="hidden" value="'.$faqid.'" name="faqid">
                                    <input type="text" class="form-control" name="answer" placeholder="Write answer"> 
                                    <input type="submit" id="" qid="'.$faqid.'" class="form-control btn btn-success" name="submit" value="Answer this question">
                                </form>
                            </div>
                          </div>
                            ';
                }
            }

            ?>

        </div>
    </div>

    <style>
        .faqHeader {
            font-size: 27px;
            margin: 20px;
        }

        .panel-heading [data-toggle="collapse"]:after {
            font-family: 'Glyphicons Halflings';
            content: "\e072"; /* "play" icon */
            float: right;
            color: #F58723;
            font-size: 18px;
            line-height: 22px;
            /* rotate "play" icon from > (right arrow) to down arrow */
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }

        .panel-heading [data-toggle="collapse"].collapsed:after {
            /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            color: #454444;
        }
    </style>

    <!-- Bootstrap FAQ - END -->

</div>
<script>
    $("body").delegate("#vote", "click", function(event) {
        event.preventDefault();
        var faqid = $(this).attr("faq");

        $.ajax({
            url: "action.php",
            method: "GET",
            data: {
                vote: 1,
                faqid: faqid
            },
            success: function(data) {
                alert(data);
            }
        })
    });
</script>

</body>
</html>