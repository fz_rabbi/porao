<?php
    include "db.php";
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Porao FAQ</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

<div class="page-header">
    <h1>Porao FAQ <small>Porao Frequently Asked Questions</small></h1>
    <h3>Hi, <?php echo $_SESSION["name"];?></h3>
</div>

<!-- Bootstrap FAQ - START -->
<div class="container">
    <br />
    <br />
    <br />
    <?php
        if(isset($_POST["submit"])){
            $name = $_POST["name"];
            if($name != ""){
                $email = $_POST["email"];
                $question = $_POST["question"];

                $sql = "insert into faq_questions(user_name, user_email, questions) values('$name', '$email', '$question')";
                $run = $conn->prepare($sql);
                $run->execute();

                echo '<script>alert("Your question is uploaded.")</script>';
                echo '<script>document.getElementById("name").value = "";</script>';
            }

        }
    ?>

    <form action="faq.php" method="post">
        <h1> <label  for="Ask">ASK ? </label> </h1>
        <hr>
        <div class="form-group">
            <label>Name : </label>
            <input type="text" name="name" class="form-control" id="name" required/>
        </div>
        <div class="form-group">
            <label>Email : </label>
            <input type="text" name="email" class="form-control" required/>
        </div>
        <div class="form-group">
            <label>Your question : </label>
            <input type="text" name="question" class="form-control" required/>
        </div>
        <input type="submit" name="submit" class="form-control btn btn-success" required/>

	</form>

    <br />
    <!--Common question-->
    <div class="panel-group" id="accordion">
        <div class="faqHeader">Common questions</div>
        <?php
            $sql = "select * from common_questions";
            $run = $conn->prepare($sql);
            $run->execute();

            if($run->rowCount() > 0){
                while ($row = $run->fetch(PDO::FETCH_ASSOC)){
                    $question = $row["question"];
                    $answer = $row["answer"];

                    echo '
                          <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#">'.$question.'</a>
                                </h4>
                            </div>
                            <div class="panel-body">
                                '.$answer.'
                            </div>
                        </div>  
                    ';
                }
            }

        ?>
        <!--Common question end-->

        <!--FAQ question-->
        <div class="faqHeader">User's Question</div>

        <?php
        $sql = "select * from faq_questions";
        $run = $conn->prepare($sql);
        $run->execute();

        if($run->rowCount() > 0){
            while ($row = $run->fetch(PDO::FETCH_ASSOC)){
                $question = $row["questions"];
                $answer = $row["answer"];
                $name = $row["user_name"];
                $email = $row["user_email"];
                $faqid = $row["id"];

                echo '
                          <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#">'.$question.' </a>
                                    <p>-by <small>'.$name.', '.$email.'</small></p>
                                </h4>
                            </div>
                            ';
                if($answer != ""){
                    echo '
                            <div class="panel-body">
                                '.$answer.'
                            </div>
                             
                    ';
                }else{
                    echo '
                            <div class="panel-body">
                                Sorry this question is not answered yet.
                            </div>
                            ';
                }
                echo '
                    <div class="panel-body">
                                <a href="#" id="vote" faq="'.$faqid.'"><i class="fa fa-thumbs-up"></i>Vote this question</a>
                            </div>
                        </div> 
                ';

            }
        }

        ?>
        <!--FAQ question end-->

        <!--Important question-->
        <div class="faqHeader">Important Question</div>

        <?php
        $sql = "select * from faq_questions";
        $run = $conn->prepare($sql);
        $run->execute();

        if($run->rowCount() > 0){
            while ($row = $run->fetch(PDO::FETCH_ASSOC)){
                $question = $row["questions"];
                $answer = $row["answer"];
                $name = $row["user_name"];
                $email = $row["user_email"];
                $faqid = $row["id"];
                $vote = $row["vote_count"];



                echo '
                          <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#">'.$question.' </a>
                                    <p><small><b>'.$vote.' people voted for this question.</b></small></p>
                                </h4>
                            </div>
                          </div>
                            ';




            }
        }

        ?>

    </div>
</div>

<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "\e072"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }
</style>

<!-- Bootstrap FAQ - END -->

</div>
<script>
    $("body").delegate("#vote", "click", function(event) {
        event.preventDefault();
        var faqid = $(this).attr("faq");

        $.ajax({
            url: "action.php",
            method: "GET",
            data: {
                vote: 1,
                faqid: faqid
            },
            success: function(data) {
                alert(data);
            
        })
    });
</script>

</body>
</html>